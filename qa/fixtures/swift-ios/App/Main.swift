//
//  Main.swift
//  App
//
//  Created by James Liu on 3/5/2023.
//

import SwiftUI
import CryptoKit

@main
struct Main: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
    
    func badHash() {
        var insecureHash = Insecure.SHA1()
    }
}
