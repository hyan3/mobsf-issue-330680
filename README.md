# MobSF analyzer

The MobSF analyzer performs SAST scanning on repositories containing code written in the following languages: `kotlin`, `swift`, `java`, and `objective-c`.

The analyzer wraps [Mobile Security Framework
(MobSF)](https://github.com/MobSF/Mobile-Security-Framework-MobSF), and is written in Go. It's structured similarly to other Static Analysis analyzers because it uses the shared [command](https://gitlab.com/gitlab-org/security-products/analyzers/command) package.

The analyzer is built and published as a Docker image in the GitLab Container Registry associated with this repository. You would typically use this analyzer in the context of a [SAST](https://docs.gitlab.com/ee/user/application_security/sast) job in your CI/CD pipeline. However, if you're contributing to the analyzer or you need to debug a problem, you can run, debug, and test locally using Docker.

For instructions on local development, please refer to the [README in Analyzer Scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/master/analyzers-common-readme.md).

## How does the analyzer work?

Setup and preprocessing for MobSF scans is a little more involved than other SAST analyzers.

* MobSF is executed as a long-running Python server at http://127.0.0.1:8000 within the
  container. The analyzer wrapper code invokes the service via a REST API for each scan.
  Booting the server is handled by the [`entrypoint.sh`](./entrypoint.sh) script.
* There are two endpoints of interest:
	* `POST /api/v1/upload` which takes a ZIP archive or a binary, and starts the scan.
	* `GET /api/v1/scan` which returns the results of a given scan.
* Scan results are a JSON payload which is transformed into an intermediate `Report`
  structure, before being converted into the `gl-sast-report.json`. This is done to
  make it easier to consolidate findings from multiple scans into a single report.
* MobSF requires ZIP archives to conform with specific directory structures depending
  on the language and project type being scanned. Read the `createScanJobs` function in
  `analyze.go` to learn more.

## Versioning and release process

Please check the [versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common#versioning-and-release-process).

## Special thanks

This feature was a generous contribution by the [H-E-B
Digital](https://digital.heb.com/) team. You can [read more H-E-B's contribution](https://about.gitlab.com/releases/2020/10/22/gitlab-13-5-released/#sast-support-for-ios-and-android-mobile-apps/)
integrating MobSF via the [GitLab Secure Scanning integration
framework](https://docs.gitlab.com/ee/development/integrations/secure.html).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
