package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/plugin"
)

func init() {
	plugin.Register("mobsf", Match)
}

// Match checks if the file is a supported MobSF scanning entrypoint
func Match(path string, info os.FileInfo) (bool, error) {
	filename := info.Name()

	if filename == "AndroidManifest.xml" {
		return true, nil
	}

	ext := filepath.Ext(filename)
	if ext == ".ipa" || ext == ".apk" || ext == ".xcodeproj" {
		return true, nil
	}

	return false, nil
}
