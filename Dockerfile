# This is the digest of the `v3.7.6` tag and was extracted using the following command:
#
#       docker buildx imagetools inspect opensecurity/mobile-security-framework-mobsf:v3.7.6
#
# Note that the Docker Hub tags page (https://hub.docker.com/r/opensecurity/mobile-security-framework-mobsf/tags) omits
# the digest for the multiarch image, which is why we need to get it with `docker buildx imagetools inspect`.
#
# Also see https://stackoverflow.com/questions/74761737/how-to-use-a-multiarch-base-docker-image-when-specifying-sha
# for more information.
ARG SCANNER_DIGEST=sha256:11affd13eaa6bc23379c139f350e3aae47a6edcc87022c2834eb9723d3f9dd26
# Follows the Code Quality naming scheme for custom versions: https://gitlab.com/gitlab-org/ci-cd/codequality#versioning-and-release-cycle
ARG SCANNER_VERSION=3.7.6-gitlab.1

ARG POST_ANALYZER_SCRIPTS_VERSION=0.2.0
ARG TRACKING_CALCULATOR_VERSION=2.4.1

FROM registry.gitlab.com/security-products/post-analyzers/scripts:${POST_ANALYZER_SCRIPTS_VERSION} AS scripts
FROM registry.gitlab.com/security-products/post-analyzers/tracking-calculator:${TRACKING_CALCULATOR_VERSION} AS tracking

FROM golang:1.19 as build

WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
  PATH_TO_MODULE=`go list -m` && \
  go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM opensecurity/mobile-security-framework-mobsf@$SCANNER_DIGEST

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION}

ENV MOBSF_API_KEY="key"
ENV MOBSF_URL="localhost:8000"

USER root
RUN apt-get update && \
  apt-get -y upgrade

COPY entrypoint.sh /entrypoint.sh

COPY --from=tracking /analyzer-tracking /analyzer-tracking
COPY --from=scripts /start.sh /analyzer
COPY --from=build /analyzer /analyzer-binary

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/analyzer", "run"]
